// Абстрактная фабрика
interface CarFactory {
    Engine createEngine();
    Transmission createTransmission();
}

// Конкретная фабрика для производства автомобилей с бензиновым двигателем
class GasolineCarFactory implements CarFactory {
    public Engine createEngine() {
        return new GasolineEngine();
    }

    public Transmission createTransmission() {
        return new ManualTransmission();
    }
}

// Конкретная фабрика для производства электромобилей
class ElectricCarFactory implements CarFactory {
    public Engine createEngine() {
        return new ElectricEngine();
    }

    public Transmission createTransmission() {
        return new AutomaticTransmission();
    }
}

// Абстрактный класс "Производственный процесс"
abstract class ManufacturingProcess {
    protected ProductionLine productionLine;

    public ManufacturingProcess(ProductionLine productionLine) {
        this.productionLine = productionLine;
    }

    public abstract void fabricate();

    public abstract void assemble();
}

// Конкретный класс, реализующий производственный процесс для создания двигателя
class EnginePartsManufacturingProcess extends ManufacturingProcess {
    public EnginePartsManufacturingProcess(ProductionLine productionLine) {
        super(productionLine);
    }

    public void fabricate() {
        productionLine.run();
        System.out.println("Создание деталей двигателя");
    }

    public void assemble() {
        productionLine.run();
        System.out.println("Сборка деталей двигателя");
    }
}

// Конкретный класс, реализующий производственный процесс для создания трансмиссии
class TransmissionManufacturingProcess extends ManufacturingProcess {
    public TransmissionManufacturingProcess(ProductionLine productionLine) {
        super(productionLine);
    }

    public void fabricate() {
        productionLine.run();
        System.out.println("Создание деталей трансмиссии");
    }

    public void assemble() {
        productionLine.run();
        System.out.println("Сборка трансмиссии");
    }
}

// Посредник, управляющий объектами-компонентами
class CarMediator {
    private Engine engine;
    private Transmission transmission;

    // Остальные компоненты машины

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    // Методы для взаимодействия между компонентами
}

// Компоненты
interface Engine {
    void start();
    void stop();
}

class GasolineEngine implements Engine {
    public void start() {
        // Запуск двигателя
    }

    public void stop() {
        // Остановка двигателя
    }
}

class ElectricEngine implements Engine {
    public void start() {
        // Запуск электродвигателя
    }

    public void stop() {
        // Остановка электродвигателя
    }
}

interface Transmission {
    void shift();
}

class AutomaticTransmission implements Transmission {
    public void shift() {
        // Переключение передачи
    }
}

class ManualTransmission implements Transmission {
    public void shift() {
        // Переключение передачи
    }
}

// Производственная линия, которая используется в процессе производства
class ProductionLine {
    // методы, реализующие производственные процессы
    public void run() {
        System.out.println("Выполняем процесс");
    }
}